package icontext

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gogf/gf/util/gconv"
	"github.com/tencentyun/scf-go-lib/functioncontext"
	"gitlab.com/zeropkg/ilog"
	"gitlab.com/zeropkg/itimer"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	HttpRequestKey = "Http-Request"
)

// imporant，这字典表很重要，表示记录的字段来自header中的哪个key，到了icontext这里又rename叫什么
var (
	logDic = map[string]string{
		"X-Uid":            "uid",
		"X-Platform":       "platform",
		"X-Openid":         "openid",
		"X-Uionid":         "unionid",
		"X-Scf-Request-Id": "RequestId",
		"X-Scf-Namespace":  "Namespace",
		"path":             "path",
		"X-Env":            "env", // 生产环境还是测试环境

		"Sec-Ch-Ua-Platform": "Platform", // 操作系统
		// 没有做rename的
		"User-Agent":      "User-Agent",
		"Accept-Encoding": "Accept-Encoding",
		"X-Client-Proto":  "X-Client-Proto",
		"X-Real-Ip":       "X-Real-Ip",
		"X-Forwarded-For": "X-Forwarded-For",
	}
)

type contextImpl struct {
	context.Context
	ginCtx *gin.Context
	ilog.ILog
	itimer.ITimeGroup

	lock sync.RWMutex
	Keys map[string]interface{}
}

func (c *contextImpl) handleWithGinContext(fields map[string]interface{}, gctxs ...*gin.Context) {
	if len(gctxs) > 0 {
		gctx := gctxs[0]
		c.ginCtx = gctx // 重要，这里是gin的context

		for k, v := range gctx.Request.Header {
			if renameKey, ok := logDic[k]; ok {
				// 如果在日志字典logDic中命中了， 就rename成对应的key塞入到fileds中
				if len(v) == 1 {
					fields[renameKey] = v[0]
					c.Set(renameKey, v[0])
				} else {
					fields[renameKey] = v
					c.Set(renameKey, v)
				}

			}

		}
		if gctx.Request.URL != nil {
			fields["path"] = fmt.Sprintf("%v:%v", gctx.Request.Method, gctx.Request.URL.Path)
		} else {
			fields["path"] = fmt.Sprintf("%v", gctx.Request.Method)
		}
	}
}

func (c *contextImpl) handleWithFunctionContext(fields map[string]interface{}, ctx context.Context) {
	if ctx != nil {
		fContext, ok := functioncontext.FromContext(ctx)
		if ok && fContext.RequestID != "" {
			fields["RequestId"] = fContext.RequestID
			c.Set("RequestId", fContext.RequestID) // 导入到Keys中
		}
		if ok && fContext.Namespace != "" {
			fields["Namespace"] = fContext.Namespace
			c.Set("Namespace", fContext.Namespace) // 导入到Keys中
		}
	}
}

// 构造方法 WithContext
func WithContext(ctx context.Context, gctxs ...*gin.Context) *contextImpl {
	c := &contextImpl{
		Context: ctx,
		Keys:    make(map[string]interface{}),
	}

	fields := make(map[string]interface{})
	c.handleWithFunctionContext(fields, ctx) // 处理腾讯云的异步函数调用需要的context
	c.handleWithGinContext(fields, gctxs...) // 处理gin的context
	c.ILog = ilog.NewLogImpl(fields)
	c.ITimeGroup = itimer.NewTimerGroup()

	return c
}

// 构造方法 NewContext
func NewContext() IContext {
	return WithContext(context.Background())
}

// 构造方法 NewContext
func WithCancel(parent context.Context) (IContext, CancelFunc) {
	switch p := parent.(type) {
	case IContext:
		return p.WithCancel()
	default:
		c, cancel := context.WithCancel(p)
		return WithContext(c), cancel
	}
}

// 构造方法 WithTimeout
func WithTimeout(goctx context.Context, d time.Duration) (IContext, CancelFunc) {
	switch p := goctx.(type) {
	case IContext:
		return p.WithTimeout(d)
	default:
		c, cancel := context.WithTimeout(p, d)
		return WithContext(c), cancel
	}
}

func (c *contextImpl) WithContext(goctx context.Context) IContext {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.Context = goctx
	c.ILog = ilog.NewLogImpl()
	c.ITimeGroup = itimer.NewTimerGroup()
	return c
}

func (c *contextImpl) WithCancel() (IContext, CancelFunc) {
	c.lock.RLock()
	goctxWithCancel, cancel := context.WithCancel(c.Context)
	newCtx := c.clone()
	c.lock.RUnlock()
	newCtx.Context = goctxWithCancel
	newCtx.ILog = ilog.NewLogImpl()
	newCtx.ITimeGroup = itimer.NewTimerGroup()
	return newCtx, cancel
}

func (c *contextImpl) WithTimeout(d time.Duration) (IContext, CancelFunc) {
	c.lock.RLock()
	goctxWithCancel, cancel := context.WithTimeout(c.Context, d)
	newCtx := c.clone()
	c.lock.RUnlock()
	newCtx.Context = goctxWithCancel
	return newCtx, cancel

}

func (c *contextImpl) TimeLog() {
	if c.ITimeGroup != nil {
		p := make(map[string]interface{}, 0)
		for key, value := range c.ITimeGroup.GetAllTime() {
			p[key] = value.String()
		}
		c.InfofWithField(p, "time used")
	}
}

func (c *contextImpl) Set(key string, value interface{}) {
	c.lock.Lock()
	if c.Keys == nil {
		c.Keys = make(map[string]interface{})
	}
	c.Keys[key] = value
	c.Context = context.WithValue(c.Context, key, value)
	c.lock.Unlock()
}

func (c *contextImpl) Get(key string) (value interface{}, exists bool) {
	c.lock.RLock()
	value, exists = c.get(key)
	c.lock.RUnlock()
	return
}

func (c *contextImpl) GetUserID() string {
	value, exists := c.Get("uid")
	if exists {
		return gconv.String(value)
	}
	return ""
}

func (c *contextImpl) GetUserObjectID() (primitive.ObjectID, error) {
	value, exists := c.Get("uid")
	if exists {
		result, err := primitive.ObjectIDFromHex(gconv.String(value))
		return result, err
	}
	return primitive.ObjectID{}, errors.New("not found uid in icontext")
}

func (c *contextImpl) GetPlatform() string {
	value, exists := c.Get("platform")
	if exists {
		return gconv.String(value)
	}
	return ""
}

func (c *contextImpl) GetNamespace() string {
	value, exists := c.Get("Namespace")
	if exists {
		return gconv.String(value)
	}
	return ""
}

func (c *contextImpl) GetRequestID() string {
	value, exists := c.Get("RequestId")
	if exists {
		return gconv.String(value)
	}
	return ""
}

func (c *contextImpl) GetUnionID() string {
	value, exists := c.Get("unionid")
	if exists {
		return gconv.String(value)
	}
	return ""
}

func (c *contextImpl) GetOpenID() string {
	value, exists := c.Get("openid")
	if exists {
		return gconv.String(value)
	}
	return ""
}

// func (c *contextImpl) GetHttpRequest() *http.Request {
// 	value, exists := c.Get(HttpRequestKey)
// 	if !exists {
// 		return nil
// 	}
// 	req, ok := value.(*http.Request)
// 	if !ok {
// 		return nil
// 	}
// 	return req
// }

func (c *contextImpl) get(key string) (value interface{}, exists bool) {
	// 从keys中获取
	value, exists = c.Keys[strings.ToUpper(key)]
	if exists {
		return
	}
	// 从context中获取
	value = c.Context.Value(key)
	if value != nil {
		return value, true
	}

	return nil, false
}

func (c *contextImpl) clone() *contextImpl {
	newCtx := &contextImpl{
		Context: c.Context,
		Keys:    make(map[string]interface{}),
	}
	for key, value := range c.Keys {
		newCtx.Keys[key] = value
	}
	return newCtx
}

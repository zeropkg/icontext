package icontext

import (
	"context"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/zeropkg/ilog"
	"gitlab.com/zeropkg/itimer"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CancelFunc = context.CancelFunc

type IContext interface {
	context.Context
	ilog.ILog
	itimer.ITimeGroup

	TimeLog()

	Set(key string, value interface{})
	Get(key string) (value interface{}, exists bool)
	// GetEnv() string
	GetNamespace() string
	GetRequestID() string
	GetUnionID() string
	GetOpenID() string
	GetUserID() string
	GetUserObjectID() (primitive.ObjectID, error)
	GetPlatform() string

	WithCancel() (IContext, CancelFunc)
	WithTimeout(d time.Duration) (IContext, CancelFunc)

	// GetHttpRequest() *http.Request
	IGin // gin的部分能力
}

type IGin interface {
	GinContext() *gin.Context
	JSON(code int, obj any)
	String(code int, format string, values ...any)
	AbortWithStatusJSON(code int, jsonObj any)
	Abort()
	Next()
}

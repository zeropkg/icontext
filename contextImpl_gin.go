package icontext

import (
	"github.com/gin-gonic/gin"
)

var (
	infoLevelHttpCodes = map[int]bool{
		200: true,
		201: true,
		202: true,
		301: true,
		302: true,
		404: true,
	}
)

func TransferFromGin(gctx *gin.Context) IContext {
	if gctx == nil {
		return NewContext()
	}
	ictx := WithContext(gctx.Copy().Request.Context(), gctx)
	// 单独把Http-Request设置成gin.Request
	return ictx
}

func (this *contextImpl) GinContext() *gin.Context {
	return this.ginCtx
}

func (this *contextImpl) JSON(httpcode int, obj any) {
	// mp := map[string]interface{}{
	// 	"call_method": "gin.JSON",
	// 	"http_code":   httpcode,
	// 	"obj":         obj,
	// }
	// if _, ok := infoLevelHttpCodes[httpcode]; ok {
	// 	this.Info(mp)
	// } else {
	// 	this.Warn(mp)
	// }

	if this.ginCtx == nil {
		return
	}
	this.ginCtx.JSON(httpcode, obj)

}

func (this *contextImpl) String(httpcode int, format string, values ...any) {
	// mp := map[string]interface{}{
	// 	"resp_method": "gin.String",
	// 	"http_code":   httpcode,
	// 	"format":      format,
	// 	"values":      values,
	// }
	// if _, ok := infoLevelHttpCodes[httpcode]; ok {
	// 	this.Info(mp)
	// } else {
	// 	this.Warn(mp)
	// }
	if this.ginCtx == nil {
		return
	}
	this.ginCtx.String(httpcode, format, values)
}

func (this *contextImpl) Abort() {
	if this.ginCtx == nil {
		return
	}
	this.ginCtx.Abort()
}

func (this *contextImpl) Next() {
	if this.ginCtx == nil {
		return
	}
	this.ginCtx.Next()
}

func (this *contextImpl) AbortWithStatusJSON(code int, jsonObj any) {
	if this.ginCtx == nil {
		return
	}
	this.ginCtx.AbortWithStatusJSON(code, jsonObj)
}

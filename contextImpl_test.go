package icontext

import (
	"context"
	"fmt"
	"reflect"
	"sync"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/tencentyun/scf-go-lib/functioncontext"
	"gitlab.com/zeropkg/ilog"
	"gitlab.com/zeropkg/itimer"
)

func atest(ictx IContext) {
	defer ictx.CostTime()()
	time.Sleep(time.Second)
}

func btest(ictx IContext) {
	defer ictx.CostTime()()
	time.Sleep(time.Second * 3)
}

func TestNewContext(t *testing.T) {
	ictx := NewContext()
	ictx.Info("hello")

	atest(ictx)
	btest(ictx)

	ictx.TimeLog()

}

func TestGetOpenID(t *testing.T) {
	gctx := GetTestGinContext()
	gctx.Request.Header.Set("X-Scf-Request-Id", "c53eaf4b-611e-45df-9e84-124325c615b8")
	gctx.Request.Header.Set("X-Openid", "123456")
	ictx := TransferFromGin(gctx)
	fmt.Println(ictx.GetOpenID())
}

func TestWithTimeout(t *testing.T) {
	ictx, cancel := WithTimeout(context.Background(), 1*time.Second)
	defer func() {
		cancel()
		ictx.Info("cancel function is executed")
	}()

	go btest(ictx)

	time.Sleep(time.Second * 3)

	ictx.TimeLog()

}

func TestWithContext(t *testing.T) {
	parent := context.Background()
	lc := &functioncontext.FunctionContext{}
	// lc := &functioncontext.FunctionContext{RequestID: "1234567"}
	type args struct {
		ctx   context.Context
		gctxs []*gin.Context
	}
	tests := []struct {
		name string
		args args
		want *contextImpl
	}{
		{
			name: "test functioncontext",
			args: args{
				ctx: functioncontext.NewContext(parent, lc),
			},
			want: &contextImpl{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := WithContext(tt.args.ctx, tt.args.gctxs...)
			got.Info("hello", "123")
			// if got := WithContext(tt.args.ctx, tt.args.gctxs...); !reflect.DeepEqual(got, tt.want) {
			// 	t.Errorf("WithContext() = %v, want %v", got, tt.want)
			// }
		})
	}
}

func Test_contextImpl_WithContext(t *testing.T) {
	type fields struct {
		Context    context.Context
		ginCtx     *gin.Context
		ILog       ilog.ILog
		ITimeGroup itimer.ITimeGroup
		lock       sync.RWMutex
		Keys       map[string]interface{}
	}
	type args struct {
		goctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   IContext
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &contextImpl{
				Context:    tt.fields.Context,
				ginCtx:     tt.fields.ginCtx,
				ILog:       tt.fields.ILog,
				ITimeGroup: tt.fields.ITimeGroup,
				lock:       tt.fields.lock,
				Keys:       tt.fields.Keys,
			}
			if got := c.WithContext(tt.args.goctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("contextImpl.WithContext() = %v, want %v", got, tt.want)
			}
		})
	}
}

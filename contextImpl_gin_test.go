package icontext

import (
	"context"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/gin-gonic/gin"
	"gitlab.com/zeropkg/ilog"
	"gitlab.com/zeropkg/itimer"
)

func GetTestGinContext() *gin.Context {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = &http.Request{
		Header: make(http.Header),
	}

	return ctx
}

func TestJSON(t *testing.T) {
	gctx := GetTestGinContext()
	ictx := TransferFromGin(gctx)
	type User struct {
		Name string `json:"name"`
		Age  int    `json:"age"`
	}
	user := &User{Name: "zhangsan", Age: 18}
	ictx.JSON(200, gin.H{"code": 0, "data": user})
	ictx.Info("hello")
}

func TestTransferFromGin1(t *testing.T) {
	gctx := GetTestGinContext()
	gctx.Request.Header.Set("X-Scf-Request-Id", "c53eaf4b-611e-45df-9e84-124325c615b8")
	gctx.Request.Header.Set("X-Uid", "123")
	gctx.Request.Header["X-Forwarded-For"] = []string{"1.1.1.1", "1.2.3.4"}
	ictx := TransferFromGin(gctx)

	ictx.Info("hello")

}

func TestGinContext(t *testing.T) {
	gctx := GetTestGinContext()
	ictx := TransferFromGin(gctx)
	spew.Dump(ictx.GinContext())
}

func TestTransferFromGin2(t *testing.T) {
	gctx := GetTestGinContext()
	gctx.Request.Header.Set("X-Scf-Request-Id", "c53eaf4b-611e-45df-9e84-124325c615b8")
	gctx.Request.Header.Set("X-Uid", "123")
	gctx.Request.Header.Set("X-Scf-Namespace", "test")
	gctx.Request.Header["X-Forwarded-For"] = []string{"1.1.1.1", "1.2.3.4"}
	ictx := TransferFromGin(gctx)

	ictx.Info("hello")

}

// func TestTransferFromGin(t *testing.T) {
// 	type args struct {
// 		gctx *gin.Context
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want IContext
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := TransferFromGin(tt.args.gctx); !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("TransferFromGin() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func Test_contextImpl_JSON(t *testing.T) {
	type fields struct {
		Context    context.Context
		ILog       ilog.ILog
		ITimeGroup itimer.ITimeGroup
		lock       sync.RWMutex
		Keys       map[string]interface{}
	}
	type args struct {
		code int
		obj  any
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &contextImpl{
				Context:    tt.fields.Context,
				ILog:       tt.fields.ILog,
				ITimeGroup: tt.fields.ITimeGroup,
				lock:       tt.fields.lock,
				Keys:       tt.fields.Keys,
			}
			this.JSON(tt.args.code, tt.args.obj)
		})
	}
}
